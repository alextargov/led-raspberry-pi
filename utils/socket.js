module.exports = {
    makeConnection: () => {
        const Gpio = require('pigpio').Gpio;

        const server = require('../app').server;
        const io = require('socket.io')(server);
        const pause = require('./pauseExecution').pause;

        const redLed = new Gpio(17, { mode: Gpio.OUTPUT });
        const greenLed = new Gpio(24, { mode: Gpio.OUTPUT });
        const blueLed = new Gpio(22, { mode: Gpio.OUTPUT });

        redLed.digitalWrite(0); // Turn RED LED off
        greenLed.digitalWrite(0); // Turn GREEN LED off
        blueLed.digitalWrite(0); // Turn BLUE LED off

        let red = 0;
        let green = 0;
        let blue = 0;
        let isCheckedBlink = false;
        let isLightTriggered = false;

        io.on('connection', (client) => {  
            console.log('Client connected...');
            let interval;
            let timeout = [];
        
            client.emit('light', { red, green, blue });
            client.emit('blink', { blinkToggle: isCheckedBlink });
        
            client.on('light', (data) => {
                isLightTriggered = true;
                clearInterval(interval);
                console.log(data);
        
                red = data.red;
                green = data.green;
                blue = data.blue;
        
                client.broadcast.emit('light', data); // emit to all but not to the initial sender
                client.emit('blink', { blinkToggle: false });
        
                redLed.pwmWrite(parseInt(red));
                greenLed.pwmWrite(parseInt(green));
                blueLed.pwmWrite(parseInt(blue));
            });
        
            const resetLights = () => {
                redLed.pwmWrite(parseInt(red));
                greenLed.pwmWrite(parseInt(green));
                blueLed.pwmWrite(parseInt(blue));
        
                clearInterval(interval);
                timeout.forEach((func) => clearTimeout(func));
        
                client.emit('blink', { blinkToggle: false });
            };
        
            const blinkFunction = () => {
                for (let i = 1; i <= 255; i += 1) {
                    if(isLightTriggered || !isCheckedBlink) {
                        resetLights();
                        return;
                    }
        
                    timeout.push(setTimeout(() => {
                        if(isLightTriggered || !isCheckedBlink) {
                            resetLights();
                            return;
                        }
        
                        redLed.pwmWrite(i);
                        greenLed.pwmWrite(i);
                        blueLed.pwmWrite(i);
        
                        if(i === 255) {
                            backwards();
                        }
                    }, i * 30));
                }

                let backwards = () => {
                    for (let i = 255; i >= 1; i -= 1) {
                        if(isLightTriggered || !isCheckedBlink) {
                            resetLights();
                            return;
                        }

                        redLed.pwmWrite(i);
                        greenLed.pwmWrite(i);
                        blueLed.pwmWrite(i);
        
                        pause(30);
                    }
                }
            };
        
            client.on('blink', (data) => {
                isCheckedBlink = data.blinkToggle;
                isLightTriggered = false;

                client.broadcast.emit('blink', data);	

                if (isCheckedBlink) {
                    blinkFunction();
                    interval = setInterval(blinkFunction, 16000);
                } else {
                    resetLights();
                }
            });
        });
        
        process.on('SIGINT', () => { //on ctrl+c
            redLed.digitalWrite(0);
            greenLed.digitalWrite(0);
            blueLed.digitalWrite(0);
            process.exit(); //exit completely
        });
    }
};
