const path = require('path');
const express = require('express');
const app = express()
const server = require('http').createServer(app);

let ip = require('./utils/serverIP').ip;
const connection = require('./utils/socket').makeConnection;

const port = 3000

app.set('views', './views');
app.set('view engine', 'pug');
app.use('/static', express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => res.render('index'));

server.listen(port, () => console.log(`Server listen on ip: ${ip}:${port}`));

module.exports = {
	server,
};

connection();