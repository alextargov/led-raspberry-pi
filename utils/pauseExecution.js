module.exports = {
    pause: (milliseconds) => {
        const now = new Date(); 
        const exitTime = now.getTime() + milliseconds;

        while (true) { 
            now = new Date(); 

            if (now.getTime() > exitTime) {
                return;
            }
        } 
    }
}