const socket = io();
let emitData = true;
let blinkToggle = false;

$(function() {
	const refreshSwatch = () => {
    	const red = $('#red').slider('value');
    	const green = $('#green').slider('value');
		const blue = $('#blue').slider('value');

		if (emitData) {
			socket.emit('light', {red, green, blue});
		}

		$('#swatch').css('background-color', `rgb(${red}, ${green}, ${blue})`);
	};

	const handleToggle = (event) => {
		blinkToggle = $(event.target).is(":checked");
		socket.emit('blink', { blinkToggle });
	};

	$('#red, #green, #blue').slider({
      	orientation: 'horizontal',
	  	range: 'min',
      	max: 255,
	  	value: 0,
      	slide: refreshSwatch,
  		change: refreshSwatch
	});

	$('.toggle').on('change', handleToggle );

	socket.on('light', (data) => {
		emitData = false;
		$('#red').slider('value', data.red);
 		$('#green').slider('value', data.green);
 		$('#blue').slider('value', data.blue);
		emitData = true;
	});

	socket.on('blink', (data) => {
		if($('#slow_blink').is(":checked") !== data.blinkToggle) {
			$('#slow_blink').prop('checked', data.blinkToggle);
		}
	});
});
